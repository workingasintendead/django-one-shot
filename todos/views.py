from django.shortcuts import render, redirect, get_object_or_404
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todo_lists(request: HttpRequest) -> HttpResponse:
    todolist = TodoList.objects.all
    context = {"todo_list": todolist}
    return render(request, "todos/list.html", context)


def todo_list_detail(request: HttpRequest, id: int) -> HttpResponse:
    todoitem = get_object_or_404(TodoList, id=id)
    context = {"todo_item": todoitem}
    return render(request, "todos/detail.html", context)


def todo_list_create(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request: HttpRequest, id: int) -> HttpResponse:
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {"form": form}
    return render(request, "todos/update.html", context)


def todo_list_delete(request: HttpRequest, id: int) -> HttpResponse:
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request: HttpRequest, id: int) -> HttpResponse:
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    context = {"form": form}
    return render(request, "todos/update_item.html", context)
